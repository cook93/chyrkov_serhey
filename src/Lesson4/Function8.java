package Lesson4;

public class Function8 {
    public static void main(String[] args) {

        double maxFunc = 0;
        double maxFuncTMP = 0;

        for (double num8 = -10; num8 <= 10; num8++) {
            if (!((num8 > -10) && (num8 < 10)))
                maxFuncTMP = Math.pow(Math.E, num8); // Math.E является константой как и значени "Пи"
            if (maxFuncTMP > maxFunc) {
                maxFunc = maxFuncTMP;
            }
        }
        System.out.println("Максимальное значение функции  " + maxFunc);
    }
}
// Для заданных функций найдите их максимальное значение и x, при котором оно достигается,
// на указанных интервалах с шагом 0.01 с использованием цикла/циклов for:
//e^x на интервале [-10, 10]