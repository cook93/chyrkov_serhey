package Lesson4;

public class Function7 {
    public static void main(String[] args) {

        double maxFunc = 0;
        double maxFuncTMP = 0;

        for (double num7 = -10; num7 <= 10; num7++) {
            if (!((num7 > -1) && (num7 < 1)))
                maxFuncTMP = 1 / Math.pow(num7, 3) + Math.pow(num7, 3);
            if (maxFuncTMP > maxFunc) {
                maxFunc = maxFuncTMP;
            }
        }
        System.out.println("Максимальное значение функции  " + maxFunc);
    }
}
// Для заданных функций найдите их максимальное значение и x, при котором оно достигается,
// на указанных интервалах с шагом 0.01 с использованием цикла/циклов for:
//  1/ x^3 + x^3 на интервалах [-10, -1] и [1, 10]