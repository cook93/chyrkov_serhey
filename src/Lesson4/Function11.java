package Lesson4;

public class Function11 {
    public static void main(String[] args) {

        double maxFunc = 0;
        double maxFuncTMP = 0;

        for (double num11 = -5; num11 <= 5; num11++) {
            if (!((num11 > -5) && (num11 < 5)))
                maxFuncTMP = Math.pow(num11,Math.E)+3;
            if (maxFuncTMP > maxFunc) {
                maxFunc = maxFuncTMP;
            }
        }
        System.out.println("Максимальное значение функции  " + maxFunc);
    }
}
// Для заданных функций найдите их максимальное значение и x, при котором оно достигается,
// на указанных интервалах с шагом 0.01 с использованием цикла/циклов for:
//x^e + x на интервале [-5, 5)
