package Lesson4;

public class Function10 {

    public static void main(String[] args) {

        double maxFunc = 0;
        double maxFuncTMP = 0;

        for (double numy = -10; numy <= 10; numy++) {
            if (!((numy > -10) && (numy < 10)))
                maxFuncTMP = Math.pow(numy, Math.E);
            if (maxFuncTMP > maxFunc) {
                maxFunc = maxFuncTMP;
            }
        }
        System.out.println("Максимальное значение функции  " + maxFunc);
    }
}
//// Для заданных функций найдите их максимальное значение и x, при котором оно достигается,
//// на указанных интервалах с шагом 0.01 с использованием цикла/циклов for:
//x^e на интервале [-10, 10]

