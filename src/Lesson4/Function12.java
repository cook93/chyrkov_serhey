package Lesson4;

public class Function12 {
    public static void main(String[] args) {
        double maxFunc = 0;
        double maxFuncTMP = 0;

        for (double num12 = -5; num12 <= 5; num12++) {
            if (!((num12 > -5) && (num12 < 5)))
                maxFuncTMP = Math.pow(num12, Math.E) + Math.pow(num12, 2);
            if (maxFuncTMP > maxFunc) {
                maxFunc = maxFuncTMP;
            }
        }
        System.out.println("Максимальное значение функции  " + maxFunc);
    }
}
//Для заданных функций найдите их максимальное значение и x, при котором оно достигается,
//// на указанных интервалах с шагом 0.01 с использованием цикла/циклов for:
//x^e + x^2 на интервале [-5, 5)