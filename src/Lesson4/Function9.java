package Lesson4;

public class Function9 {
    public static void main(String[] args) {

        double maxFunc = 0;
        double maxFuncTMP = 0;

        for (double num9 = -100; num9 <= 100; num9++) {
            if (!((num9 > -100) && (num9 < 100)))
                maxFuncTMP = Math.pow(Math.E, Math.sin(num9));
            if (maxFuncTMP > maxFunc) {
                maxFunc = maxFuncTMP;
            }
        }
        System.out.println("Максимальное значение функции  " + maxFunc);
    }
}
//// Для заданных функций найдите их максимальное значение и x, при котором оно достигается,
//// на указанных интервалах с шагом 0.01 с использованием цикла/циклов for:
//e^sin(x) на интервале [-100, 100]
