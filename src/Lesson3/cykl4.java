package Lesson3;

import java.sql.SQLOutput;
import java.util.Scanner;

public class cykl4 {
    public static void main(String[] args) {

    Scanner Vally = new Scanner(System.in); // scanner Vally - vally название сканнера
    while (true) {  // <<while пока>> выполняет условие (в данном случае True обозначает что условие всегда выполняется и
    // является правдой, а не <<false ложь>> )

    System.out.println("Введите число от 0 до 59"); // прошу пользователя ввести некое число от и до

    int min = Vally.nextInt(); // обьявил переменную min сканнер Vally выполняет действие с
    // консоли где ввожу данные переменной min


    if (0 <= min && min <= 15) {
        System.out.println("первая четверть часа"); //
    } else if (15 <= min && min <= 30) {     // <<иначе-else>>  <<если if>> принято писать на одной строчке!
        System.out.println("вторая четверть часа");
    } else if (30 <= min && min <= 45) {   //написал для удобства своего
        System.out.println("третья четверть часа");
    } else if (45 <= min && min <= 60) {
        System.out.println("четвертая четверть часа");
        }
    }

    }
}

//В переменной min лежит число от 0 до 59. Определите в какую четверть часа попадает это число (в первую, вторую,
// третью или четвертую).
