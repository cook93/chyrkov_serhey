package Lesson3;

import java.util.Scanner;

public class cykl5 {
    public static void main(String[] args) {

    System.out.println("Введите число от 1 до 4");

    Scanner Valy2 = new Scanner (System.in);

    int num = Valy2.nextInt();
    switch(num) {

        case 1:
            System.out.println("Зима");
            break;

        case 2:
            System.out.println("Весна");
            break;

        case 3:
            System.out.println("Лето");
            break;

        case 4:
            System.out.println("Осень");
            break;

        default: // если ни одно значение выше не является удоволетворящим наш запрос
            System.out.println("Неверное значение");

    }
    }
}
//Переменная num может принимать 4 значения: 1, 2, 3 или 4. Если она имеет значение '1', то в переменную result запишем 'зима'
// , если имеет значение '2' – 'весна' и так далее. Решите задачу через switch-case.